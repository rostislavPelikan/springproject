package cz.pelikan.java.itNetwork.arena;

import cz.pelikan.java.itNetwork.service.Mage;
import cz.pelikan.java.itNetwork.service.Warrior;

public class Arena {
	Warrior warr1;
	Warrior warr2;

	public Arena(Warrior warr1, Warrior warr2) {
		this.warr1 = warr1;
		this.warr2 = warr2;
	}

	public void msg(String string) {
		System.out.println(string);
	}

	public void start() {
		System.out.println("Nastupuji bojovnici:");
		System.out.println(warr1.getName() + " \t\t:\t " + warr2.getName());
		System.out.println(warr1.graphic() +"\t" + warr2.graphic());
		while (warr1.alive() && warr2.alive()) {
			warr1.attack(warr2);
			warr2.attack(warr1);
			
		System.out.println(warr1.graphic());
		System.out.println(warr2.graphic());
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		if (!warr1.alive()) {
			System.out.printf("Bojovnik %s umrel.",warr1.getName());
		}
		else {
			System.out.printf("Bojovnik %s umrel.",warr2.getName());
		}
	}

}

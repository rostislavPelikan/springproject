package cz.pelikan.java.itNetwork.service;
import java.util.Random;
import com.sun.xml.internal.bind.v2.runtime.Name;

import cz.pelikan.java.itNetwork.arena.Arena;

public class Warrior {
	Random random = new Random();
	static Cube cube = new Cube(10);
	//zivot bojovnika 
	int hp;
	//maximalni zivot
	int maxHp;
	public int getHp() {
		return hp;
	}
	//utok
	int ofense;
	//obrana
	int defense;
	//jmeno
	String name;
	public Warrior(String name,int maxHP,int ofense,int defense) {
		this.name  = name;this.defense = defense;this.ofense = ofense;
		maxHp= maxHP; hp = maxHP;
	}
	//utok
	public void attack(Warrior warrior) {
		int attack = ofense + random.nextInt(cube.getWallCount()+1);
		System.out.println(name + " utoci silou"+ attack);
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		warrior.deff(attack);
	}
	//obrana
	protected void deff(int attack) {
		int def= defense+random.nextInt(cube.getWallCount()+1);
		int dmg = attack -def;
		if(dmg>0) {
			hp -=dmg;
			System.out.println(name + " se brani "+ def + "\n utrpel poskozeni " + dmg);
		}
		else{
			System.out.println(name + " odrazil utok.");		
		}
		System.out.println(name + " se braní "+ def);
		if (dmg>0) {
		System.out.println(name +" utrpel poškození " + dmg);
		}	
		else {
			System.out.println(name+" odvrátil utok");
		}
	}
	public String getName() {
		return name;
	}

	public String graphic() {
		//stringBuffer pro vypsani zivota
		StringBuffer sb = new StringBuffer("[");
		//pocet dilku zbyvajiciho zivota
		int  index = (int)((double)hp/(maxHp/20));
		for (int i = 0; i < index; i++) {
			sb.append("#");
		}
		for (int i = index; i < 20; i++) {
			sb.append(" ");
		}
		sb.append("]");
		return sb.toString();
	}
	public boolean alive() {
		return (hp>0);
	}
}

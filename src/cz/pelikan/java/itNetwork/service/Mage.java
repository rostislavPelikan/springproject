package cz.pelikan.java.itNetwork.service;
//Mage dedi z Warriora
public class Mage extends Warrior {
	int mana;
	int maxMana;
	///konstruktor musi volat konstruktor predka
	public Mage(String name, int maxHP, int ofense, int defense, int mana) {
		super(name, maxHP, ofense, defense);
		this.mana = maxMana = mana;
	}
	@Override
	public String graphic() {
		//stringBuffer pro vypsani zivota
		StringBuffer sb = new StringBuffer("[");
		//pocet dilku zbyvajiciho zivota
		int  index = (int)((double)mana/(maxMana/20));
		for (int i = 0; i < index; i++) {
			sb.append("#");
		}
		for (int i = index; i < 20; i++) {
			sb.append(" ");
		}
		sb.append("]");
		//prevedeni stringBufferu na string
		return super.graphic()+ " mana: "+sb.toString();
		//volani graphic z pradka pro vypsani zivote, pote vypsani many
	}

	@Override
	public void attack(Warrior warrior) {
		int attack = 0;
		//prepsana cast metody pro magicky utok
		if (mana >= maxMana) {
			attack = ofense * 2 + random.nextInt(cube.getWallCount() + 1);
			System.out.println(name + " uto�� magick�m �tokem" + attack);
			mana = 0;
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			warrior.deff(attack);
		} else {
			mana += 20;
			// volani metody predka attack() pro normalni utok
			super.attack(warrior);
		}
	}

	public int getMana() {
		return mana;
	}


}

package cz.pelikan.java.itNetwork.initial;

import cz.pelikan.java.itNetwork.arena.Arena;
import cz.pelikan.java.itNetwork.service.Mage;
import cz.pelikan.java.itNetwork.service.Warrior;

public class MainClass {
	public static void main(String[] args) {
		Warrior warr1 =new Warrior("pelli", 100, 20, 10);
		Warrior warr2 = new Mage("raelli", 80, 15, 15,100);
		System.out.println("Nastupuji bojovnici:");
		System.out.println(warr1.getName()+ " : "+ warr2.getName());
		Arena arena =  new Arena(warr1,warr2);
		arena.start();
	}
}
